#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "trie.h"

#ifndef LOGI
#define LOGI(...) fprintf(stdout, __VA_ARGS__)
#endif
#ifndef LOGE
#define LOGE(...) fprintf(stderr, __VA_ARGS__)
#endif

/*
 * Create the separator string that will be passed
 * to strtok.
 *
 * We include all non-alphanumeric characters except
 * '-' b/c that's special, and apostrophe b/c things
 * like isn't should be treated as a single word
 */
static char *create_separator_str()
{
	int indx = 0;
	char *result = calloc(257, sizeof(*result));
	for (unsigned char i = 1; i < 255; i++) {
		if (isalnum(i)) {
			continue;
		} else if (i == '-') {
			continue;
		} else if (i == '\'') {
			continue;
		}
		result[indx++] = i;
	}
	return result;
}

/*
 * Strip leading and trailing hyphens from a word
 * so --foobar will just be foobar, but foo-bar
 * will be it's own word
 */
static char *strip_hyphens(char *word)
{
	char *copy = word, *tail = NULL;
	int i = 0;
	while (copy[i] == '-') { i++; }
	copy = &copy[i];
	tail = copy + strlen(copy) - 1;
	i = strlen(copy) - 1;
	while (*tail == '-') {
		*tail = '\0';
		tail--;
	}
	return copy;
}

/*
 * Parse a single line and update the trie
 */
static int parse_line(char *line, char *sep, struct trie *t, int line_num)
{
	char *saveptr;
	char *word;

	while ((word = strtok_r(line, sep, &saveptr)) != NULL) {
		//line should be NULL after
		//the first call to strtok_r
		line = NULL;
		word = strip_hyphens(word);
		if (strlen(word) == 0) {
			continue;
		}
		if (trie_insert(t, word, line_num) != 0) {
			LOGE("trie_insert failed\n");
			return -1;
		}
	}
	return 0;
}

/*
 * Loop through a file at a time updating the trie
 */
static int parse_file(FILE *fp, struct trie *t)
{
	char *line = NULL, *sep = NULL;
	size_t len = 0;
	int rc = 0;
	errno = 0;

	sep = create_separator_str();
	if (sep == NULL) {
		LOGE("out of memory?\n");
		return -1;
	}

	int line_num = 1;
	while (getline(&line, &len, fp) != -1) {
		char *c;
		c = strchr(line, '\n');
		*c = '\0';	
		if (parse_line(line, sep, t, line_num++) != 0) {
			free(line);
			free(sep);
			return -1;
		}
		errno = 0;
	}
	if (errno != 0) {
		LOGE("failed to read file: %s\n", strerror(errno));
		rc = -1;
	}
	free(line);
	free(sep);
	return rc;
}

int main(int argc, char **argv)
{
	if (argc != 2) {
		LOGE("usage: %s <input_file>\n", argv[0]);
		exit(-1);
	}

	/*
	 * open the input file
	 */
	FILE *fp = fopen(argv[1], "re");
	if (fp == NULL) {
		LOGE("failed to open file: %s\n", strerror(errno));
		exit(-1);
	}

	/*
	 * create the trie for tracking the words/line numbers
	 */
	struct trie *t = trie_alloc();
	if (t == NULL) {
		LOGE("trie_alloc failed!\n");
		exit(-1);
	}

	/*
	 * parse the file
	 */
	if (parse_file(fp, t) != 0) {
		LOGE("failed to parse file\n");
		exit(-1);
	}

	/*
	 * Print out the words/line numbers
	 */
	if (trie_dump(t) != 0) {
		LOGE("trie_dump failed\n");
		exit(-1);
	}

	/*
	 * cleanup the memory allocated for the trie
	 */
	if (trie_cleanup(t) != 0) {
		LOGE("trie_cleanup failed\n");
		exit(-1);
	}

	fclose(fp);
	return 0;
}
