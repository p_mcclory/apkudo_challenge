# Concordance

Generates a concordance for a given text file.  See [Challenge Description](challenge.txt) for the full description.

I used a [trie](https://en.wikipedia.org/wiki/Trie) as the underlying data structure for my implementation.  I
discuss my logic for doing so (including time/space complexity analysis) in [design](design.txt)

## Assumptions

I've made the following assumptions:

1. All input files are ASCII
2. hyphen's at the beginning or end of the word should be stripped and not considered part of the word, whereas
   hyphens in the middle should be retained.  i.e.:

   --foobar, foobar--, and --foobar-- are all just the word "foobar"
   whereas
   foo-bar is the word "foo-bar"

## Building

Just run:

```
make
```

I've tested this on machine running Fedora 25, with the following kernel and gcc version:

```
➜  apkudo_challenge git:(master) ✗ uname -a
Linux localhost.localdomain 4.8.6-300.fc25.x86_64 #1 SMP Tue Nov 1 12:36:38 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux

➜  apkudo_challenge git:(master) ✗ gcc --version
gcc (GCC) 6.3.1 20161221 (Red Hat 6.3.1-1)
Copyright (C) 2016 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

## Running

Just run and it will output the concordance of that file

```
./concordance <path/to/input_file>
```

i.e.

```
➜  apkudo_challenge git:(master) ✗ ./concordance tests/small_input.txt
an 3
example 3
is 1 3
it 2
kind 2
of 2 3
some 2
text 2 3
this 1
```

## Test files

I've included a few test files under tests/:

* small_input.txt - This is simply the example from the challenge description
* large_simple_input.txt - This is the words "deadbeef" and "foobar" alternating line by line - it's 79K
* really_large_simple_input.txt - Same as above but it's 77M
* ge.txt - This is a text file containing the novel Great Expectations from Project Gutenberg
* rj.txt - This is a text file containing the play Romeo and Juilet from Project Gutenberg
* metamorphasis - This is a text file containing the novel The Metamorphasis from Project Gutenberg

## Performance

My implementation seems to be reasonably fast for procerssing the novels, some runtimes on my laptop:

rj.txt:
.181 seconds

metamorphasis.txt:
.174 seconds

ge.txt:
.735 seconds

