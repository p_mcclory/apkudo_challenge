#define _GNU_SOURCE //asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define DEBUG 0

#ifndef LOGI
#define LOGI(...) fprintf(stdout, __VA_ARGS__)
#endif
#ifndef LOGE
#define LOGE(...) fprintf(stderr, __VA_ARGS__)
#endif

#if DEBUG
#define LOGD(...) fprintf(stdout, __VA_ARGS__)
#else
#define LOGD(...)
#endif

#include "trie.h"

#define NUM_PRINTABLE 95
#define PRINTABLE_OFFSET 0x20

/*
 * An individual trie node
 */
struct trie {
	char *prefix;
	//this is a bit wasteful on memory
	//but makes inserts fast
	struct trie *desc[NUM_PRINTABLE];
	// does this prefix represent a word itself
	int terminal;
	//an array of ints, one for each line this word appears on
	unsigned int *line_nums;
	//the number of elements in line_nums
	unsigned int line_count;
	//the current capacity of line_nums
	unsigned int line_capacity;
};


/*
 * Trie traversal
 * performs the pre_callback when it first visits the
 * node IFF the node is - this is for in order traveral
 * (i.e. actually dumping the words)
 * performs the post_callback after visiting all descendents
 * (so depth first traversal) - this is used for freeing
 * the tree (can't access the descendents if we free the
 *  parent first)
 */
static int trie_traverse(struct trie* t,
			 int (*pre_callback)(struct trie *),
			 int (*post_callback)(struct trie *))
{
	if (t->terminal) {
		if (pre_callback) {
			if (pre_callback(t) != 0) {
				return -1;
			}
		}
	}
	//loop through all descendents
	for (int i = 0; i < NUM_PRINTABLE; i++) {
		/*
		 * If there's a valid descendent
		 * recurse
		 */
		if (t->desc[i]) {
			if (trie_traverse(t->desc[i],
					  pre_callback,
					  post_callback) != 0) {
				return -1;
			}
		}
	}
	if (post_callback) {
		if (post_callback(t) != 0) {
			return -1;
		}
	}
	return 0;
}


/*
 * Allocate a new trie structure
 */
struct trie *trie_alloc()
{
	struct trie *t = calloc(1, sizeof(*t));
	if (t == NULL) {
		LOGE("out of memory!\n");
		return NULL;
	}

	t->line_nums = calloc(64, sizeof(*(t->line_nums)));
	if (t->line_nums == NULL) {
		free(t);
		LOGE("out of memory!\n");
		return NULL;
	}
	t->line_count = 0;
	t->line_capacity = 64;
	return t;
}

/*
 * Free the memory associated with a single trie node
 */
static int trie_cleanup_one(struct trie *t)
{
	if (t == NULL) {
		return -1;
	}
	free(t->prefix);
	t->prefix = NULL;
	t->terminal = 0;
	free(t->line_nums);
	t->line_nums = NULL;
	free(t);
	return 0;
}

/*
 * Recursively free a trie
 */
int trie_cleanup(struct trie *t)
{
	return trie_traverse(t, NULL, &trie_cleanup_one);
}

/*
 * Insert the given word into the tree with the associated line number
 */
int trie_insert(struct trie *t,
		const char *word,
		unsigned int line_num)
{
	if (t == NULL || word == NULL) {
		LOGE("%s: invalid params\n", __func__);
		return -1;
	}

	LOGD("attempting to insert %s line %d in trie node %s\n",
	     word, line_num, t->prefix);

	if (line_num == 0) {
		LOGE("%s: line_num must be non-zero\n", __func__);
		return -1;
	}

	/*
	 * We found a match,
	 * mark it as terminal and update the line numbers
	 */
	if (strlen(word) == 0) {
		LOGD("adding %d to node %s\n", line_num, t->prefix);
		t->terminal = 1;
		/*
		 * We need to expand the line_nums array, double it's size
		 */
		if (t->line_capacity == t->line_count) {
			t->line_nums = realloc(t->line_nums,
					       sizeof(unsigned int) * t->line_capacity * 2);
			if (t->line_nums == NULL) {
				LOGE("out of memory!\n");
				return -1;
			}
			t->line_capacity *= 2;
		}
		/*
		 * If there's already been some lines for this word check that this
		 * line isn't already there.  We just check the last one.  This assumes
		 * in order insertion....which is fine we're the only caller of this
		 * right now
		 */
		if (t->line_count > 0 && t->line_nums[t->line_count-1] == line_num) {
			LOGD("already have %s in line %d, skipping",
			     word, line_num);
			return 0;
		} else {
			t->line_nums[t->line_count] = line_num;
			t->line_count += 1;
			return 0;
		}
	} else {
		/*
		 * No match need to recurse
		 */
		int indx = word[0] - PRINTABLE_OFFSET;
		if (isalpha(word[0])) {
			indx = tolower(word[0]) - PRINTABLE_OFFSET;
		}
		/*
		 * The descendent node doesn't exist yet
		 */
		if (t->desc[indx] == NULL) {
			t->desc[indx] = trie_alloc();
			if (t->desc[indx] == NULL) {
				LOGE("out of memory\n");
				return -1;	
			}
			if (t->prefix == NULL) {
				if (asprintf(&(t->desc[indx]->prefix),
					     "%c", tolower(word[0])) == -1) {
					LOGE("out of memory\n");
					return -1;
				}
			} else {
				if (asprintf(&(t->desc[indx]->prefix), "%s%c",
					     t->prefix, tolower(word[0])) == -1) {
					LOGE("out of memory\n");
					return -1;
				}
			}
		}
		return trie_insert(t->desc[indx], word + 1, line_num);
	}

	return 0;
}

/*
 * Print the word and line numbers for a single trie node
 */
static int trie_dump_one(struct trie *t)
{
	if (t == NULL) {
		return -1;
	}
	LOGI("%s ", t->prefix);
	for (unsigned int i = 0; i < t->line_count; i++) {
		LOGI("%d ", t->line_nums[i]);
	}
	LOGI("\n");
	return 0;
}

/*
 * Recursively dump the trie to stdout
 */
int trie_dump(struct trie* t)
{
	return trie_traverse(t, &trie_dump_one, NULL);
}

