CFLAGS := -Wall -Wextra -Werror
CFLAGS += -ggdb -O0 #debugging

all: trie_test concordance

concordance: concordance.c trie.o
	$(CC) $(CFLAGS) -o $@ $^

trie_test: trie_test.c trie.o
	$(CC) $(CFLAGS) -o $@ $^

trie.o: trie.c trie.h
	$(CC) $(CFLAGS) -c $<

clean:
	rm -rf *.o trie_test concordance
