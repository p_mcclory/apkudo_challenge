#ifndef TRIE_H
#define TRIE_H

struct trie;

/*
 * Create a new empty trie
 */
struct trie *trie_alloc(void);

/*
 * Recursively free the memory used by a trie
 */
int trie_cleanup(struct trie*);

/*
 * Insert a word/line number pair into the trie
 */
int trie_insert(struct trie *,
		const char *word,
		unsigned int line_num);

/*
 * Print contents of the trie to stdout
 */
int trie_dump(struct trie*);

#endif
