#include <stdio.h>
#include <stdlib.h>

#include "trie.h"

#ifndef LOGI
#define LOGI(...) fprintf(stdout, __VA_ARGS__)
#endif
#ifndef LOGE
#define LOGE(...) fprintf(stderr, __VA_ARGS__)
#endif

int main(void)
{
	struct trie *t = trie_alloc();
	if (t == NULL) {
		LOGE("trie_alloc failed!\n");
		exit(-1);
	}

	/*
	 * This is
	 * some kind OF text it
	 * Is an example of text
	*/
	char *line_1[] = { "This", "is", "1994", NULL };
	char *line_2[] = { "some", "kind", "OF", "text", "it", NULL};
	char *line_3[] = { "Is", "an", "example", "of", "text", NULL};
	char **lines[] = { line_1, line_2, line_3 };

	for (int i = 0; i < 3; i++ ) {
		char **line = lines[i];
		char **str = line;
		while (*str) {
			if (trie_insert(t, *str, i + 1) != 0) {
				LOGE("trie_insert failed\n");
				return -1;
			}
			str = str + 1;
		}
	}

	LOGI("trie:\n");
	if (trie_dump(t) != 0) {
		LOGE("trie_dump failed\n");
		exit(-1);
	}

	if (trie_cleanup(t) != 0) {
		LOGE("trie_cleanup failed\n");
		exit(-1);
	}

	return 0;
}
